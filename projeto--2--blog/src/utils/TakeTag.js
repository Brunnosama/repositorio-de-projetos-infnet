import axios from '../axios/Axios'

const takeTag = async (saveState) => {

// const takeTag = saveState => {
    
//     //Mock
//         const _tagList = [
//         {
//             "id": 1,
//             "description": "Game Events"
//         },
//         {
//             "id": 2,
//             "description": "Roleplaying Games"
//         },
//         {
//             "id": 3,
//             "description": "Trading Card Games"
//         },
//         {
//             "id": 4,
//             "description": "Video Games"
//         }
//     ];

    try {
        const anwser = await axios.get('tag');
        const _tagList = anwser.data;

        _tagList.sort( (a, b) => {
            return (a.description > b.description) ? 1 : -1;
    } );

    saveState (_tagList);
}   catch(error) {
    console.log (`An Error has occurred: ${error.message}.`);}

};

//     _tagList.sort( (a, b) => {
//         return (a.description > b.description) ? 1 : -1;
//     }
// );

//     saveState (_tagList);

export default takeTag;