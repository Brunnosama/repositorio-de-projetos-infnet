import axios from "../axios/Axios"

const deletePostService = async (idPost) => {

    // console.log (`idPost: ${idPost}`);

    try {
        const anwser = await axios.delete(`post/${idPost}`)

            return {
                'success': true,
                'message': 'Post deleted.'
                };
    }
    catch(error) {
        
        return {
            'success': false,
            'message': 'There was a problem while deleting the post.'
            };
        }
    }


export default deletePostService