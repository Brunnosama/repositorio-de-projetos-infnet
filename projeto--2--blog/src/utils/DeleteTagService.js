import axios from "../axios/Axios"

const deleteTagService = async (id) => {

    // console.log (`idTag: ${idTag}`);

    try {
        const anwser = await axios.delete(`tag/${id}`)

            return {
                'success': true,
                'message': 'Tag deleted.'
                };
    }
    catch(error) {
        
        return {
            'success': false,
            'message': 'There was a problem while deleting the tag.'
            };
        }
    }


export default deleteTagService;