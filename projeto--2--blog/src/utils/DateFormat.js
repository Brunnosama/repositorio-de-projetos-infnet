    //Original data example: 2012-04-27
    //Result: 27/04/2012

const dateFormat = (originalDate) => {

    const day = originalDate.substring (8, 10)
    const month = originalDate.substring (5, 7)
    const year = originalDate.substring (0, 4)
    const newDate = `${day}/${month}/${year}`;

    return newDate;
}

export default dateFormat;