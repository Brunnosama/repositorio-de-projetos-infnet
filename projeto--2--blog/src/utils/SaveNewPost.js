import axios from '../axios/Axios'

const saveNewPost = async (newPost, setMessage) => {

    try {
        const anwser = await axios.post('post', newPost);

        setMessage ('New post added!');
        // setMessage(anwser.data);
    } 
    catch(error) {
        setMessage ('There was a problem adding a new post')

        switch (error.response.status) {
            case 409:
                setMessage(error.response.data.message);
                break;
            
            default:
                setMessage('There was a problem while adding the post')    
        }
    }

};

export default saveNewPost;