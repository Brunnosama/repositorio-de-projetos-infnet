import React, {useState, useEffect} from 'react';
import takePost from './TakePost';


const PostNewId = () => {
    
    const [post, setPostId] = useState([]);
    
    useEffect (()=> { 
        takePost( setPostId);

        
            const lastPostIndex = post.length -1;
            const lastPost = post[lastPostIndex];
            const lastPostId = lastPost.id;
            const newPostId  = parseInt (lastPostId) +1;
        
        setPostId(newPostId)
;}, [post]);
}

export default PostNewId;
    
//     const lastPostIndex = post.length -1;
//     const lastPost = post[lastPostIndex];
//     const lastPostId = lastPost.id;
//     const newPostId  = (lastPostId) +1;
    
//     console.log(newPostId);
// }



// const takePost = async (saveState) => {

//     try {
//         const anwser = await axios.get('post')
        
//         saveState (anwser.data);
//     } catch (error) {
//         console.log (`An Error has occurred: ${error.message}.`);

//     }


//     // saveState (_postList)

// };

// export default takePost;