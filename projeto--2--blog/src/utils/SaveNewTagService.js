import axios from '../axios/Axios';

const saveNewTagService = async (newTag, setMessage) => {

    try {
        const anwser = await axios.post('tag', newTag);

        alert('New Tag created.');
    }
    catch (error) {
        
        if (error.response && error.response.status) {
            switch(error.response.status) {

                case 409:
                    setMessage (error.response.data.message);
                default:
                    setMessage ('There was a error while communicating with the server.');
                break;
            }
        }

        setMessage ('The server seems to be offline...')
    }

};

export default saveNewTagService;

