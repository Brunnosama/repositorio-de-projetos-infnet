import axios from "../axios/Axios";

const takePostDetailsService = async (id) => {

    try {
        const answer = await axios.get(`post/${id}`);
        console.log(answer);

        return answer.data.post;
    } 
    catch(e) {
        alert ('There was a problem.')
    }


};

export default takePostDetailsService;