//use to refer to the colors of the theme

const COLOR_DEFAULT ='white';
const COLOR_1 ='pink';
const COLOR_2 ='green';
const COLOR_3 ='blue';

const COLOR_DEFAULT_CONFIG = {
    themeBackgroundColor: 'white',
    colorBckgBttn: 'green',
    colorTextBttn: 'white'
    };

const COLOR_1_CONFIG = {
    themeBackgroundColor: 'lightpink',
    colorBckgBttn: '#ff5c75',
    colorTextBttn: 'black'
    };

const COLOR_2_CONFIG = {
    themeBackgroundColor: 'greenyellow',
    colorBckgBttn: '#5e8a1b',
    colorTextBttn: 'white'
    };

const COLOR_3_CONFIG = {
    themeBackgroundColor: 'darkcyan',
    colorBckgBttn: '#006161',
    colorTextBttn: 'white'
    };

export {
    COLOR_DEFAULT,
    COLOR_1,
    COLOR_2,
    COLOR_3
} ;

export {
    COLOR_DEFAULT_CONFIG,
    COLOR_1_CONFIG,
    COLOR_2_CONFIG,
    COLOR_3_CONFIG
} ;