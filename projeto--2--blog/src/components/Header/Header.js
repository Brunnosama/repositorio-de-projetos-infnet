import React from 'react';
import './Header.css';
import MainMenu from './MainMenu/MainMenu';

import {COLOR_DEFAULT, COLOR_1, COLOR_2, COLOR_3} from '../../utils/ThemeColors'

const Header = ({funcConfigTheme}) => {
    return (
        <header>
            <div id='h-title-theme'>

                <h1>Header</h1>

                <div id='h-themes'>

                    <p>Themes:</p> 

                    <button 
                    id='h-theme-pink'
                    onClick={ () => {funcConfigTheme(COLOR_1) } }>
                        Pink
                    </button>

                    <button 
                    id='h-theme-green'
                    onClick={ () => {funcConfigTheme(COLOR_2) } }>
                        Green
                    </button>

                    <button 
                    id='h-theme-blue'
                    onClick={ () => {funcConfigTheme(COLOR_3) } }>
                        Blue
                    </button>

                    <button 
                    id='h-theme-white'
                    onClick={ () => {funcConfigTheme(COLOR_DEFAULT) } }>
                        White
                    </button>
                    
                </div>
                
            </div>
            
            <MainMenu/>

        </header>
    );
};

export default Header;