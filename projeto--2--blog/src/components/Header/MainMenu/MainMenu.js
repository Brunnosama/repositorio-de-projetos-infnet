import React, {useContext} from 'react';
import './MainMenu.css';

import {Link} from 'react-router-dom'; 

import ThemeContext from '../../../contexts/ThemeContext';

const MainMenu = () => {

    const theme= useContext(ThemeContext); // ThemeContext.Consumer

    return (
        <nav style= {{backgroundColor: theme.themeBackgroundColor}}>

            <ul>
                <li>
                    <Link to='/post-list'>Post</Link>
                </li>
                <li>
                    <Link to='/tag-list'>Tags</Link>
                </li>
                <li>
                    <Link to='/new-post'>New Post</Link>
                </li>

            </ul>

        </nav>
    );
};

export default MainMenu;