import React, {useState, useEffect} from 'react';
import './CoreContent.css';
import { Route, Switch, Redirect } from 'react-router-dom';

//Pages
import PostList from './PostList/PostList';
import TagList from './TagList/TagList';
import NewPost from './NewPost/NewPost';
import PostDetails from './PostDetails/PostDetails'
import PostByTag from './PostByTag/PostByTag';
import NewTag from './NewTag/NewTag';

import takeTag from '../../utils/TakeTag';
import takePost from '../../utils/TakePost';


const CoreContent = () => {
    
    const [tag, setTag] = useState ([]);
    const [post, setPost] = useState ([]);

    useEffect( () => {
        takeTag(setTag);
        }, [] );
    
    useEffect( () => {
        takePost(setPost);
        }, [] );
    
    return (
        <main>

            <Switch>

                <Route exact path="/">
                    <Redirect to= "/post-list" />
                </Route>

                <Route path="/post-list">
                    <PostList list = {post}/>
                </Route>
                
                <Route path="/tag-list">
                    <TagList list ={tag}/>
                </Route>
                
                <Route path="/new-post">
                    <NewPost tags = {tag}/>
                </Route>

                <Route path="/post-details/:id" component={PostDetails} />

                <Route path="/post-by-tag/:tagId" component={PostByTag} />
                
                <Route path="/new-tag/" component={NewTag} />

            </Switch>

        </main>
    );
};

export default CoreContent;