import React, {useState} from 'react';
import saveNewPost from '../../../utils/SaveNewPost';
import PostList from '../PostList/PostList';

import './NewPost.css';

const NewPost = ({tags}) => {

    const [title, setTitle] = useState ('');
    const [tag, setTag] = useState ('');
    const [imageUrl, setImageUrl] = useState ('');
    const [imageAlt, setImageAlt] = useState ('');
    const [description, setDescription] = useState ('');
    const [author, setAuthor] = useState ('');
    const [company, setCompany] = useState ('');
    const [system, setSystem] = useState ('');
    const [confirmed, setMessage] = useState ('');

    const addNewPost = event => {

        console.log ('\'Submitted\' form');

    event.preventDefault();

        if ( !title || !tag || !imageUrl || !description || !author || !company || !system) {
            alert ('Fill in all required fields.');
            return false;
        }

        const today = new Date();

        const year = today.getFullYear ();
        const month = today.getMonth ();
        const day = today.getDate();

        const formattedMonth = (month + 1) < 10 ? "0" + String ((month + 1)) : String ((month + 1));


        // // Gerando o id do novo item
        // const indiceUltimaAtividade = listaAtividade.length -1;
        // const ultimaAtividade = listaAtividade[indiceUltimaAtividade];
        // const idUltimaAtividade = ultimaAtividade.id;
        // const idNovaAtividade = parseInt (idUltimaAtividade) +1;
        // const novaAtividade = {
        //         "id": idNovaAtividade,
        
        //Mount object to send to the service.
        const newPost = {
            
            // 'id': newPostId,
            'tagId': tag,
            'title': title,
            'imageURL': imageUrl,
            'imageAlt': imageAlt,
            'description': description,
            'postDate': `${year}-${formattedMonth}-${day}`,
            'authors': author,
            'company': company,
            'system': system
        };

        // Call the service and pass the new post object.
        saveNewPost (newPost, setMessage);

    };

    return (
        <div id='new-post'>

            <h4>New Post</h4>

            {confirmed ? <p  id='np-confirmed'>{confirmed}</p> : null}

            <form onSubmit ={event => addNewPost(event)}>
            
            <div className = 'np-field'>
                <label htmlFor='np-title-field'>Title</label>
                <input
                    id='np-title-field'
                    name='np-title-field' 
                    value={title}
                    onChange={event => setTitle(event.target.value)} />
            </div>
            
            <div className = 'np-field'>
                <label htmlFor='np-tag-field'>Tag</label>
                <select
                    id='np-tag-field'
                    name='np-tag-field'
                    value={tag}
                    onChange={event => setTag(event.target.value)} >
                        <option value= {-1} disabled >Select a Tag</option>
                        
                        {tags.map(item => {
                            return <option value={item.id} key={[item.id]}>
                                    {item.description}
                                    </option>
                            })}
                </select>
            </div>
            <div className = 'np-field'>
                <label htmlFor='np-imageURL-field'>Image URL</label>
                <input
                    id='np-imageURL-field'
                    name='np-imageURL-field'
                    value={imageUrl} 
                    onChange={event => setImageUrl(event.target.value)} />
            </div>
            <div className = 'np-field'>
                <label htmlFor='np-imageAlt-field'>Image Alt</label>
                <input
                    id='np-imageAlt-field'
                    name='np-imageAlt-field'
                    value={imageAlt} 
                    onChange={event => setImageAlt(event.target.value)} />
            </div>
            <div className = 'np-field'>
                <label htmlFor='np-description-field'>Description</label>
                <textarea
                    id='np-description-field'
                    name='np-description-field'
                    value={description} 
                    onChange={event => setDescription(event.target.value)} />
            </div>
            <div className = 'np-field'>
                <label htmlFor='np-author-field'>Authors</label>
                <input
                    id='np-author-field'
                    name='np-author-field'
                    value={author} 
                    onChange={event => setAuthor(event.target.value)} />
            </div>
            <div className = 'np-field'>
                <label htmlFor='np-company-field'>Company</label>
                <input
                    id='np-company-field'
                    name='np-company-field'
                    value={company} 
                    onChange={event => setCompany(event.target.value)} />
            </div>
            <div className = 'np-field'>
                <label htmlFor='np-system-field'>System</label>
                <input
                    id='np-system-field'
                    name='np-system-field'
                    value={system} 
                    onChange={event => setSystem(event.target.value)} />
            </div>

            <button>
                Send
            </button>

            </form>
            

        </div>
    )
};


export default NewPost;
