import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import './TagList.css';

import deleteTagService from '../../../utils/DeleteTagService';

const TagList = ({list}) => {

    const [deletedTag, setDeletedTag] = useState (false);

    const deleteTag = id => {

        const answer = confirm('Are you sure?');

        if (answer) {
            const returnService = deleteTagService(id);
            if (returnService.success) {
                setDeletedTag(true);
                return false;
            };
        }
    

    return (
        
        <div id='tag-list'>

            <div id ='tl-title-bttn-box'>

                <h3 id='tl-title'>Tag List</h3>

                <Link to ='new-tag' id='tl-new-tag-bttn'>
                    New Tag
                </Link>
                
            </div>

            <ul id= 'tl-tags'>
                {list.map (item =>
                    <li className ='tl-item' key={item.id}>
                        {item.description}
                        <button 
                        className='tl-item-delete'
                        onClick={() => {deleteTag(item.id)}}>
                            Delete</button>
                        <Link 
                        className='tl-item-post-list'
                        to= {`post-by-tag/${item.id}`}>
                            List Post
                        </Link>
                    </li>)}
            </ul>
        </div>
    );
}
}

export default TagList;