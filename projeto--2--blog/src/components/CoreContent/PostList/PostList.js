import React from 'react';

import './PostList.css';
import Post from './Post/Post';

const PostList = ({list}) => {
    return (
        <div>
            {list.length > 0 ?
            list.map (item => <Post key={item.id} post={ item } /> )
            : <p>Loading...</p>
            }
        </div>
        
    );
};

export default PostList;