import React, {useContext, useState} from 'react';
import { Link } from 'react-router-dom';
import './Post.css';
import ThemeContext from '../../../../contexts/ThemeContext';
import dateFormat from '../../../../utils/DateFormat';
import deletePostService from '../../../../utils/DeletePostService';

const Post = ( {post}) => {

    const theme = useContext(ThemeContext);

    const [deletedPost, setDeletedPost] = useState (false);

    //call the servicce to delete the post and pass the id:


    const deletePost = async () => {
        const result = await deletePostService (post.id);
        if (result.success) {
            alert (result.message);
            setDeletedPost (true);
            return false;
        }
        alert (result.message);
};

    //Resultado: DD/MM/AAAA

    return (

        <>
        {deletedPost ?
        
        null
        
        :
        
        <article
        className='post'
        style= {{backgroundColor: theme.themeBackgroundColor}}>

            <div className="p-title-delete-box">
            
                <h3 className="p-title">
                    {post.title}
                </h3>
                <button 
                className='p-btn-delete'
                onClick={event => deletePost()} >
                    Delete
                </button>
                
            </div>

            <p className='p-posted-on'>
                Posted on: {dateFormat (post.postDate)}
            </p>

            <div className='p-text-img'>

                <div className='p-img'>
                    <img
                        src= {post.imageURL}
                        alt= {post.imageAlt} />
                </div>
                
                <div className='p-text'>
                    {post.description}
                </div>

            </div>

            <div className ='p-tag-bttn'>
                <div className='p-tag'>
                    Tag: {post.tagId}
                </div>

                <Link
                    to={`/post-details/${post.id}`}
                    className='p-keep-reading-bttn'
                    style={{
                            backgroundColor: theme.colorBckgBttn,
                            color: theme.colorTextBttn
                            }}
                    >
                        Keep Reading...
                </Link>
            </div>

        </article>
        
        }
        </>
    );
};

export default Post;