import React, {useEffect, useState} from 'react';
import './PostDetails.css';
import {useParams} from 'react-router-dom';

import takePostDetailsService from '../../../utils/TakePostDetailsService';

const PostDetails = () => {

    const {id} = useParams();
    
    const [loadedPost, setLoadedPost] = useState ({});

    useEffect(async ()=> {
        const _loadedPost = await takePostDetailsService(id);
        setLoadedPost (_loadedPost);
    }, []);

    return (
        <div id= 'cc-post-details'>

            <h2>
                #{loadedPost.id} - {loadedPost.title}
            </h2>

            <h4>
                {loadedPost.tagId} - {loadedPost.postDate} - {loadedPost.authors}
            </h4>

            <img src= {loadedPost.image} alt= {loadedPost.imageAlt}/>

            <div>
                {loadedPost.description}
            </div>

            <h3>
                {loadedPost.company} - {loadedPost.system}
            </h3>        

        </div>
    );
};

export default PostDetails;