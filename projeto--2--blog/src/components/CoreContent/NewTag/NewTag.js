import React, {useState} from 'react';
import './NewTag.css';
import saveNewTagService from '../../../utils/SaveNewTagService';

const NewTag = ({tags}) => {

    const [tagName, setTagName] = useState ('');
    const [message, setMessage] = useState ('');
    
    const saveNewTag = event => {
        
        event.preventDefault();

        if  (!tagName) {
            alert ('Please, fill the Tag name.');
            return false;
        }

        const newTag = {
            "tag": tags
        };

        saveNewTagService (newTag, setMessage);
    }

    
        // // Gerando o id do novo item
        // const indiceUltimaAtividade = listaAtividade.length -1;
        // const ultimaAtividade = listaAtividade[indiceUltimaAtividade];
        // const idUltimaAtividade = ultimaAtividade.id;
        // const idNovaAtividade = parseInt (idUltimaAtividade) +1;
        // const novaAtividade = {
        //         "id": idNovaAtividade,

        // // Generating the 'Id' of the New Post
        // const lastPostIndex = listaAtividade.length -1;
        // const ultimaAtividade = listaAtividade[indiceUltimaAtividade];
        // const idUltimaAtividade = ultimaAtividade.id;
        // const idNovaAtividade = parseInt (idUltimaAtividade) +1;
        // const novaAtividade = {
        //         "id": idNovaAtividade,

        //Mount object to send to the service.

    return (

        <div id='nt'>

            <h4>New Tag</h4>

            {message ? <p id='nt-message'></p>:null}

            <form onSubmit={ event => saveNewTag(event)}>
                    <div className='nt-field'>
                        <label>
                            Tag Name</label>
                        <input
                            id='nt-tag'
                            name='nt-tag'
                            onChange={event => setTagName (event.target.value)} />
                    </div>

                    <button id='nt-save-btn'>
                            Save</button>
            </form>
            
        </div>
    )
};


export default NewTag;
