import React, {useState, useEffect} from 'react';
import './PostByTag.css';
import {useParams} from 'react-router-dom';
import takePost from '../../../utils/TakePost';

const PostByTag = () => {

    const {tagId} = useParams();

    const [post, setPost] = useState([]);
    const [filteredPost, setFilteredPost] = useState([]);

    useEffect (()=> { 
        takePost( setPost);
    }, [] );

    useEffect ( ()=> {
        if (post.length >0) {
        const _filteredPost = post.filter(item => item.tagId === parseInt(tagId) );

        setFilteredPost (_filteredPost);
        }
    }, [post, tagId] );
    
    return (
        <div id= 'cc-post-by-tag'>

            <h2>Post by Tag</h2>

            <p>Selected Id: {tagId}</p>

            <div>
                {filteredPost.map (item =>{
                    return <p key={item.id}>
                                {item.id} - {item.tagId} - {item.description}
                            </p>
                })}
            </div>

        </div>
    );
};

export default PostByTag;