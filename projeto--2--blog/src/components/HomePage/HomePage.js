import React, {useState} from 'react';
import {BrowserRouter} from 'react-router-dom';

import './HomePage.css';

import Header from '../Header/Header';
import CoreContent from '../CoreContent/CoreContent';
import SideBar from '../SideBar/SideBar';
import Footer from '../Footer/Footer';
import ThemeContext from '../../contexts/ThemeContext';

import {
    COLOR_1,
    COLOR_2,
    COLOR_3,
    COLOR_DEFAULT_CONFIG,
    COLOR_1_CONFIG,
    COLOR_2_CONFIG,
    COLOR_3_CONFIG} from '../../utils/ThemeColors'


const HomePage = () => {

    const [theme, setTheme] = useState(COLOR_DEFAULT_CONFIG);
    
    const changeTheme = selectedTheme => {
    
        switch(selectedTheme) {
            
            case COLOR_1:
                setTheme (COLOR_1_CONFIG);
            break;
            
            case COLOR_2:
                setTheme (COLOR_2_CONFIG);
            break;
            
            case COLOR_3:
                setTheme (COLOR_3_CONFIG);
            break;
            
            default:
                setTheme (COLOR_DEFAULT_CONFIG);
        }
    }
    
    return (
        <ThemeContext.Provider value={theme}>
            <BrowserRouter>
            
                <div id='box-home-page'> 

                    <Header funcConfigTheme= {changeTheme} />

                <div id='container'>
                    <CoreContent/>
                    <SideBar/>
                </div>

                <Footer/>

                </div>
                
            </BrowserRouter>
        </ThemeContext.Provider>
    );
};

export default HomePage;