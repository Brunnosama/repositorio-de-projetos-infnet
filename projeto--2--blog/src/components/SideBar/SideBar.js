import React, {useState, useEffect} from 'react';
import './SideBar.css';
import Search from './Search/Search'
import TagList from './TagList/TagList'
import takeTag from '../../utils/TakeTag';


const SideBar = () => {

    const [tag, setTag] = useState ([]);

    useEffect( () => {
        takeTag(setTag);
            }, [] );

    return (
        <aside>

            <Search />
            <TagList list={tag}/>

        </aside>
    );
};

export default SideBar;