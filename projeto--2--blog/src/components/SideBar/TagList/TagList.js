import React from 'react';
import {Link} from 'react-router-dom';
import './TagList.css';


const TagList = ({list}) => {
    return (
        <div id='sb-tag-list'>

            <h4>Tags</h4>

            <ul>
            {list.map (item => {
                return <li 
                        className = 'sb-tag-item'
                        key = {item.id}>
                        <Link to = {`/post-by-tag/${item.id}`}>
                        {item.description}
                        </Link>
                    </li>;})}
            </ul>
        </div>
    );
};

export default TagList;