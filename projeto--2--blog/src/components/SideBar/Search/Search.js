import React from 'react';
import './Search.css';

const Search = () => {
    return (
        <div id='search'>

            <input 
                id='s-search-field'
                placeholder= 'Search here...' />

            <button id='s-search-bttn'>
                <img src='/images/icone-busca.png' alt='A lupe as the search icon'/>
            </button>

        </div>
    );
};

export default Search;