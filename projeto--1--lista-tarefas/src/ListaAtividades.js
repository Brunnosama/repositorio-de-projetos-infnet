import React, { useEffect, useState } from 'react';

import instanciaAxios from './ajax/instanciaAxios';

import './ListaAtividades.css'

const ListaAtividades = () => {

    const [listaPesquisa, setListaPesquisa] = useState([]);
    const [listaAtividade, setListaAtividade] = useState([]);
    const [listaRegiao, setListaRegiao] = useState([]);
    const [listaDia, setListaDia] = useState([]);
    const [listaHorario, setListaHorario] = useState([]);
    const [pesquisaNovaAtividade, setPesquisaNovaAtividade] = useState('');
    const [descricaoNovaAtividade, setDescricaoNovaAtividade] = useState('');
    const [regiaoNovaAtividade, setRegiaoNovaAtividade] = useState('');
    const [diaNovaAtividade, setDiaNovaAtividade] = useState('');
    const [horarioNovaAtividade, setHorarioNovaAtividade] = useState('');
    const [alertaNovaAtividade, setAlertaNovaAtividade] = useState('desligado');
    
    useEffect(() => {
        pegarPesquisa();
        pegarAtividade();
        pegarRegiao();
        pegarDia();
        pegarHorario();
    }, []);
    
    const pegarPesquisa = async () => {
        try {
            const resposta = await instanciaAxios.get('../json/pesquisa.json');
            // `Texto: ${ JSON.stringify(target.value) }`;
            // Para mostrar texto e variável de um json em texto
            setListaPesquisa(resposta.data.pesquisa);
            //  setXXX(event.target.value)
        }
        catch (error) {
            console.log(error.message);
        }
    };

    const pegarAtividade = async () => {
        try {
            const resposta = await instanciaAxios.get('../json/atividade.json');
            setListaAtividade(resposta.data.atividade);
        }
        catch (error) {
            console.log(error.message);
        }
    };

    const pegarRegiao = async () => {
        try {
            const resposta = await instanciaAxios.get('../json/regiao.json');
            setListaRegiao(resposta.data.regiao);
        }
        catch (error) {
            console.log(error.message);
        }
    };

    const pegarDia = async () => {
        try {
            const resposta = await instanciaAxios.get('../json/dia.json');
            setListaDia(resposta.data.dia);
        }
        catch (error) {
            console.log(error.message);
        }
    };

    const pegarHorario = async () => {
        try {
            const resposta = await instanciaAxios.get('../json/horario.json');
            setListaHorario(resposta.data.horario);
        }
        catch (error) {
            console.log(error.message);
        }
    };

    const OpcoesPesquisaComponente = () => {

        if (listaPesquisa.length > 0) {
        const listaPesquisaJSX = listaPesquisa.map((item) => { 
            return (
                <option
                    key={item.id}
                    value={item.id}>
                    { item.descricao}
                </option>
            );
        }
    );

    return listaPesquisaJSX;
    } else 
    { return null};
};



    const OpcoesDiaComponente = () => {

    if (listaDia.length > 0) {
    const listaDiaJSX = listaDia.map((item) => { 
        return (
            <div key={item.id}>
                <input 
                type='radio' 
                name='dia' 
                value={item.id}
                id={`dia-${item.valor}`}
                onChange= { (evento) => setDiaNovaAtividade (evento.target.value)}
                checked={item.id === diaNovaAtividade} /> 
                <label htmlFor={`dia-${item.valor}`}>
                    {item.descricao}
                </label>
            </div>
            
        );
    }
);

return listaDiaJSX;
} else 
{ return null};
};

const OpcoesRegiaoComponente = () => {

    if (listaRegiao.length > 0) {
    const listaRegiaoJSX = listaRegiao.map((item) => { 
        return (
            <div key={item.id}>
                <input 
                type='radio' 
                name='regiao' 
                value={item.id}
                id={`regiao-${item.valor}`}
                onChange= { (evento) => setRegiaoNovaAtividade (evento.target.value)}
                checked={item.id === regiaoNovaAtividade} /> 
                <label htmlFor={`regiao-${item.valor}`}>
                    {item.descricao}
                </label>
            </div>
            
        );
    }
);

return listaRegiaoJSX;
} else 
{ return null};
};

    const OpcoesHorarioComponente = () => {

    if (listaHorario.length > 0) {
    const listaHorarioJSX = listaHorario.map((item) => { 
        return (
            <div key={item.id}>
                <input 
                type='radio' 
                name='horario' 
                value={item.id}
                id={`horario-${item.valor}`}
                onChange= { (evento) => setHorarioNovaAtividade (evento.target.value)}
                checked={item.id === horarioNovaAtividade} /> 
                <label htmlFor={`horario-${item.valor}`}>
                    {item.descricao}
                </label>
            </div>
            
        );
    }
);

return listaHorarioJSX;
} else 
{ return null};
};

    const AlertaIconeAtividade = () => {
        return (
            
            <img
            className="alertaIcone"
            src = '/images/despertador-icone.png'
            alt='Ícone de Alerta'  
                style = { { width: '14px', marginLeft: '15px'} }/>

                )
    }

    const CorpoTabelaComponente = () => {


        if (listaAtividade.length > 0) {
            return (

                <tbody>
                    {listaAtividade.map( (item) => {
                        return (
                            <LinhaTabelaComponente
                                key={item.id}
                                id= {item.id}
                                descricaoProperty={item.descricao}
                                alertaProperty={item.alerta}
                                pesquisaProperty={item.idPesquisa} 
                                regiaoProperty={item.idRegiao}
                                diaProperty={item.idDia}
                                horarioProperty={item.idHorario}/>
                        );
                    }
                )
            }
                </tbody>
            );
        } else 
        { return null};
    };

    const LinhaTabelaComponente = (props) => {

        //Pesquisa encontrada para a atividade em questão
        const _pesquisa = listaPesquisa ? listaPesquisa.find( item => item.id === props.pesquisaProperty) : null;
        
        const _alerta = props.alertaProperty === 'ligado' ? <AlertaIconeAtividade/> : null

        const _regiao = listaRegiao ? listaRegiao.find( item => item.id === props.regiaoProperty) : null;

        //Dia encontrato para a tarefa em questão
        const _dia = listaDia ? listaDia.find( item => item.id === props.diaProperty) : null;
        
        const _horario = listaHorario ? listaHorario.find( item => item.id === props.horarioProperty) : null;

    return (
            <tr>
                <td className="descricaoAtividade">
                    {props.descricaoProperty}
                    {_alerta}
                </td>
                <td>{_regiao ? _regiao.descricao : null}</td>
                <td>{_pesquisa ? _pesquisa.descricao : null}</td>
                <td>{_dia ? _dia.descricao : null}</td>
                <td>{_horario ? _horario.descricao : null}</td>
                <td>
                    <img 
                    src = '/images/remover-icone.png'
                    className="removerIcone"
                    alt='Ícone de Lixeira'  
                    onClick = { () => { removerAtividade( props.id) } }/>
                </td>
            </tr>
        );
    };

    const incluirAtividade = () => {

        if (pesquisaNovaAtividade >0 && descricaoNovaAtividade) {
            
        // Gerando o id do novo item
            const indiceUltimaAtividade = listaAtividade.length -1;
            const ultimaAtividade = listaAtividade[indiceUltimaAtividade];
            const idUltimaAtividade = ultimaAtividade.id;
            const idNovaAtividade = parseInt (idUltimaAtividade) +1;
            const novaAtividade = {
                    "id": idNovaAtividade,
                    "descricao": descricaoNovaAtividade,
                    "idPesquisa": pesquisaNovaAtividade,
                    "idRegiao": regiaoNovaAtividade,
                    "idDia": diaNovaAtividade,
                    "idHorario": horarioNovaAtividade,
                    "alerta": alertaNovaAtividade
                };

    setListaAtividade ([...listaAtividade, novaAtividade]);

        } else {
            alert('Por favor, escolha o tipo de pesquisa e descreva a atividade')}
    };

    const removerAtividade = (idSelecionado) => {
            // console.log(`O id selecionado foi: ${idSelecionado}`)
            const _listaAtividade = listaAtividade.filter( (item) => {
                return item.id !== idSelecionado;
            }
        );

    setListaAtividade (_listaAtividade);

    };

    return (
        <> {/* ReactFragment - precisa existir pois todo componente React precisa de um componente pai */}
            
            <h1>Agenda de Pesquisa<br /> do Patrimônio Pernambucano</h1>
            
            <div id='container'>
                
                <div id='box-nova-atividade'>

                    <h3>Nova Atividade</h3>

                    <div className='campo-nova-atividade-1'>
                        <label> Tipo de Pesquisa: </label>
                        <select
                            value={pesquisaNovaAtividade}
                            onChange= { (evento) => setPesquisaNovaAtividade (evento.target.value)} >
                            <option value={-1} >Selecione o tipo de Pesquisa</option>
                            < OpcoesPesquisaComponente />
                        </select>
                    </div>

                    <div className='campo-nova-atividade-1'>
                        <label>Descrição da Atividade: </label>
                        <input 
                            type='text'
                            onChange= { (evento) => setDescricaoNovaAtividade (evento.target.value)} />
                        {/* <p>{pesquisaNovaAtividade}</p> */}
                    </div>

                    <div id='regiao-dia-horario'>

                        <div className='campo-nova-atividade-2'>
                                <label>Dia: </label>

                                <OpcoesDiaComponente/>

                        </div>

                        <div className='campo-nova-atividade-2'> 
                            <label>Região: </label>

                            <OpcoesRegiaoComponente/>

                        </div>

                        <div className='campo-nova-atividade-2'>
                            <label>Horário: </label>

                            <OpcoesHorarioComponente/>

                        </div>

                    </div>
                    
                    <div className='campo-nova-atividade-2'>
                        <span>Alerta?</span><br/>
                        <input
                            type='checkbox'
                            id='campo-alerta'
                            name='campo-alerta'
                            onChange={ () => {setAlertaNovaAtividade (
                                alertaNovaAtividade === 'ligado' ? 'desligado' : 'ligado' ) }  }/>
                        <label htmlFor='campo-alerta'>Ligado</label>
                    </div>

                    <button 
                    id='botao-nova-atividade'
                    onClick={ () => incluirAtividade() }>
                        Incluir
                    </button>
                </div>
                
                <div id='box-lista-atividades'>
                    <table>
                        <thead>
                            <tr>
                                <th>Atividade</th>
                                <th>Região</th>
                                <th>Tipo de Pesquisa</th>
                                <th>Dia</th>
                                <th>Turno</th>
                                <th>Excluir</th>
                            </tr>
                        </thead>
                        
                        <CorpoTabelaComponente />
                        
                        <tfoot>
                            <tr>
                                <td colSpan='6'>Atividades: {listaAtividade.length}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </>
    )
};

export default ListaAtividades;