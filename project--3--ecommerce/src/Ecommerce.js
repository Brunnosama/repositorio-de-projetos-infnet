import React from 'react';

import CoreContent from './features/CoreContent/CoreContent';
import Footer from './features/Footer/Footer';
import Header from './features/Header/Header';
import SideBar from './features/SideBar/SideBar';

const Ecommerce = () => {

    const containerStyle = {
        display: 'flex',
        padding: '20px',
        };

    return <>
        <Header/>
        <div id='container' style ={containerStyle}>
            
            <SideBar />
            <CoreContent />


        </div>
        <Footer />
    
    </>
};


export default Ecommerce;
