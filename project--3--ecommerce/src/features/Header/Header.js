import React from 'react';
import MainMenu from './MainMenu/MainMenu';

const Header = () => {

    const style ={
        'span': {
            fontWeight: 'bold'
        },
        'welcomeLogo': {
            display: 'flex'
        }
    };

    return <>
        <header>
            <div style={style.welcomeLogo}>
                <p>
                    Welcome, <span style ={style.span}>User</span>.
                </p>
                <p>
                    Favorites (132)
                </p>
                <p>
                <i className="fas fa-user"></i> Logout
                </p>
            </div>
            <MainMenu/>
        </header>
    </>
};

export default Header;