import React from 'react';

const MainMenu = () => {

    return <>
        <nav>
            <ul id='main-menu'>
                <li>
                    Notebooks
                    <ul className='mm-submenu'>
                        <li>Ultrathin</li>
                        <li>2 in 1</li>
                        <li>Gaming</li>
                    </ul>
                </li>
                <li>Accessories</li>
            </ul>

            <form>
                <input 
                    id='mm-search'
                    name='mm-search'
                    placeholder='Search' />
                <button>Search</button>
            </form>
        </nav>
        
    </>
};

export default MainMenu;