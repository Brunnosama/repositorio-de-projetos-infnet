import React from 'react';
import {useSelector} from 'react-redux';

const Register = () => {

    const userIsLogged = useSelector ( state => state.user.userIsLogged );
    const userLogged = useSelector ( state => state.user.userLogged );
    const tag = useSelector (state => state.tags.list);

    const [title, setTitle] = useState ('');
    const [price, setPrice] = useState ('');
    const [description, setDescription] = useState ('');
    const [tag, setTag] = useState ('');
    const [image, setImage] = useState ('');
    const [featuredImage, setFeaturedImage] = useState ('');

    return <>

        {userIsLogged && userLogged.tipo === '1' ?
            
            <>
                <h1>Register Screen</h1>

                <form onSubmit ={ e=> e.preventDefault()}>
                    <div className='pr-field'>
                        <label htmlFor='title'> Title: </label>
                        <input 
                        type='text' 
                        id='title' 
                        name='title'
                        onChange ={ e => setTitle (e.target.value)}/>
                        

                    </div>
                    <div className='pr-field'>
                        <label htmlFor='price'> Price: </label>
                        <input 
                        type='text' 
                        id='price' 
                        name='price'
                        onChange ={ e => setPrice (e.target.value)}/>

                    </div>
                    <div className='pr-field'>
                        <label htmlFor='description'> Description: </label>
                        <input 
                        type='text' 
                        id='description' 
                        name='description'
                        onChange ={ e => setDescription (e.target.value)}/>

                    </div>
                    <div className='pr-field'>
                        <label htmlFor='tag'> Tag: </label>
                        <select 
                        id='tag' 
                        name='tag'
                        onChange ={ e => setTag (e.target.value)}>
                            {tag.map (tag => <option value={tag}>
                                {tag}
                            </option>)}
                        </select>

                    </div>
                    <div className='pr-field'>
                        <label htmlFor='image'> Image: </label>
                        <input 
                        type='text' 
                        id='image' 
                        name='image'
                        onChange ={ e => setImage (e.target.value)}/>

                    </div>
                    <div className='pr-field'>
                        <label htmlFor='featured_image'> Featured Image: </label>
                        <input 
                        type='text' 
                        id='featured_image' 
                        name='featured_image'
                        onChange ={ e => setFeaturedImage (e.target.value)}/>

                    </div>
                    <button
                        className = 'primary-bttn'
                        onClick = () => {}  >
                            Save
                        </button>
                </form>
            </>
            :

            <p>You're not authorized to access this page.</p>
        }
    </>;

};

export default Register;