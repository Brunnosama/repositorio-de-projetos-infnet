import React from 'react';

const SideBar = () => {

    return <>
        <aside>
            <ul>
                <li>States
                    <p>
                        <input 
                            type='checkbox' name='condition' valune='new' /> New
                    </p>
                    <p>
                        <input 
                            type='checkbox' name='condition' valune='used' /> Used
                    </p>
                </li>
                <li>RAM
                    <p>
                        <input type='checkbox' name='ram' valune='4' /> 4GB
                    </p>
                    <p>
                        <input type='checkbox' name='ram' valune='8' /> 8GB
                    </p>
                    <p>
                        <input type='checkbox' name='ram' valune='16' /> 16GB
                    </p>
                </li>
            </ul>
        </aside>
    </>
};

export default SideBar;