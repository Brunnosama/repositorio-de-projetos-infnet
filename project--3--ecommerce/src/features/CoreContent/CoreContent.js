import React from 'react';

import ProductList from './ProductList/ProductList';

const CoreContent = () => {

    return <>
        <main>
            <ProductList />
        </main>
    </>
};

export default CoreContent;