## API:
### Criar o arquivo package.json

npm init -y

### Gerenciar as requisições, rotas e URLs, entre outras funcionalidades

npm install express

npm install nodemon cors body-parser path morgan

### Scripts package.json

"scripts": {
    "start": "node index.js",
    "dev": "nodemon index.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },

### Executar o projeto

npm run dev

## POSTMAN:

GET => localhost:3001

{
    "name": "api_brunno_pessoa",
    "version": "0.1.0"
}

## TAGS:

# createTag

POST => localhost:3001/v1/tag

# getAllTag

GET => localhost:3001/v1/tag

# getTagById

GET => localhost:3001/v1/tag/:id

# updateTag

PUT => localhost:3001/v1/tag/:id

# deleteTag

DELETE => localhost:3001/v1/tag/:id

## POSTS:

# createPost

POST => localhost:3001/v1/post

# getAllPost

GET => localhost:3001/v1/post

# getPostById

GET => localhost:3001/v1/post/:id

# updatePost

PUT => localhost:3001/v1/post/:id

# deletePost

DELETE => localhost:3001/v1/post/:id

