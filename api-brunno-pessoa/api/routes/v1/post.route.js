const postController = require('../../controllers/post.controller');

module.exports = (router) => {

    router
        .route('/post')
        .post(
            postController.createPost
        ),

    router
        .route('/post')
        .get(
            postController.getAllPost
        ),

    router
        .route('/post/:id')
        .get(
            postController.getPostById
        ),

    router
        .route('/post/:id')
        .put(
            postController.updatePost
        ),

    router
        .route('/post/:id')
        .delete(
            postController.deletePost
        )

}





