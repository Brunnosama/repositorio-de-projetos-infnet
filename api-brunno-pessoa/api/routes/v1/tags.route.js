const tagsController = require('../../controllers/tags.controller');

module.exports = (router) => {

    router
        .route('/tag')
        .post(
            tagsController.createTag
        );
    
    router
        .route('/tag')
        .get(
            tagsController.getAllTag
        );

    router
        .route('/tag/:id')
        .get(
            tagsController.getTagById
        );

    router
        .route('/tag/:id')
        .put(
            tagsController.updateTag
        ),

    router
        .route('/tag/:id')
        .delete(
            tagsController.deleteTag
        )
    
}
