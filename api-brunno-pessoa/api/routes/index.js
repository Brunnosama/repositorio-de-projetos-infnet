const { Router } = require('express');
const { name, version } = require('../../package.json');

const routesV1Tags = require('./v1/tags.route');
const routesV1Post = require('./v1/post.route');

module.exports = (app) => {

    app.get('/', (req, res, next) => {

        res.send({ name, version });
    });

    const routesV1= Router();

    routesV1Tags(routesV1);

    routesV1Post(routesV1);

    app.use('/v1', routesV1);
}