const tags = [
    {
        "id": "1",
        "description": "Game Events",
    },
    {
        "id": "2",
        "description": "Roleplaying Games",
    },
    {
        "id": "3",
        "description": "Trading Card Games",
    },
    {
        "id": "4",
        "description": "Video Games",
    },
]

module.exports = tags