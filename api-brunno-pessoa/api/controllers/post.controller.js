const post = require('../models/post.model');

const createPost = async (req, res, next) => {
    
    try {
        const body = req.body;
    
        const model = {
            id: body.id,
            tagId: body.tagId,
            title: body.title,
            imageURL: body.imageURL,
            imageAlt: body.imageAlt,
            description: body.description,
            postDate: body.postDate,
            authors: body.authors,
            company: body.company,
            system: body.system
        };
    
        await post.push(model);
    
        res.status(200).send({ message: "The Post was successfully created." });
    
      } catch (error) {
    
        res.status(500).send({ message: "Internal server error!" });
      }
};


const getAllPost = (req, res, next) => {

    return res.status(200).send(post);

}

const getPostById = (req, res, next) => {

    const id = req.params.id;

     const resultDB = post.find((result) => result.id === id) 
        
        return res.status(200).send(resultDB);

} 

const updatePost = (req, res, next ) => {

  const id = req.params.id;

  post[id] = req.body;

  res.status(200).send(post[id]);

}


const deletePost = (req, res, next) => {

  const id = req.params.id;

  post.splice(id); 

  res.status(200).send({
      
      message: "The Post was successfully deleted.",

  });
  
};


module.exports = {
    createPost,
    getAllPost,
    getPostById,
    updatePost,
    deletePost,
}