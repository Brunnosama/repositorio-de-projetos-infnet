const tags = require('../models/tags.model');


const createTag = async (req, res, next) => {

    try {
        const body = req.body;
    
        const model = {
          id: body.id,
          description: body.description,
        };
    
        await tags.push(model);
    
        res.status(200).send({ message: "A Tag foi criada com sucesso." });
    
      } catch (error) {
    
        res.status(500).send({ message: "Internal server error!" });
      }
    };
 

const getAllTag = (req, res, next) => {

    return res.status(200).send(tags);

};


const getTagById = (req, res, next) => {

    const id = req.params.id;

     const resultDB = tags.find((result) => result.id === id) 
        
        return res.status(200).send(resultDB);
}


const updateTag = (req, res, next ) => {

  const id = req.params.id;

  tags[id] = req.body;

  res.status(200).send(tags[id]);

}


const deleteTag = (req, res, next) => {

  const id = req.params.id;

  tags.splice(id, 1); 

  res.status(200).send({
      
      message: "A Tag foi excluída com sucesso.",

  });
};

  
module.exports = {
    createTag,
    getAllTag,
    getTagById,
    updateTag,
    deleteTag,
}